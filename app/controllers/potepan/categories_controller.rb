class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @taxonomies = Spree::Taxonomy.includes(root: { children: :children })
    @products = @taxon.all_products.includes(master: [:default_price, :images])
  end
end
