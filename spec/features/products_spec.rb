RSpec.feature "Products", type: :feature do
  given!(:product) { create(:product) }
  scenario 'User can view basic product information including name, price and description' do
    visit potepan_product_path(id: product.id)
    expect(page).to have_selector '.media-body', text: product.name
    expect(page).to have_selector '.page-title', text: product.name
    expect(page).to have_selector '.breadcrumb', text: product.name
    expect(page).to have_selector '.media-body', text: product.price
    expect(page).to have_selector '.media-body', text: product.description
  end
end
