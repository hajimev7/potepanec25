RSpec.feature "Categories", type: :feature do
  given!(:taxon) { create(:taxon) }
  given!(:taxonomy) { create(:taxonomy) }
  given!(:product) { create(:product) }

  background do
    taxonomy.root.children << taxon
    taxon.products << product
    visit potepan_category_path(id: taxon.id)
  end

  scenario 'User can jump home page to click BIGBAG and HOME' do
    within(".breadcrumb") do
      click_link("HOME")
    end
    within(".navbar-collapse") do
      click_link("HOME")
    end
    find(".navbar-brand").click
    visit potepan_path
  end

  scenario 'User can jump category page to click its name' do
    click_link "taxon (#{taxon.all_products.count})"
    visit potepan_category_path(id: taxon.id)
  end

  scenario 'User can view categories of products' do
    expect(page).to have_content "taxonomy"
    expect(page).to have_content "taxon"
  end

  scenario 'User can view taxon`s products information including name and price' do
    expect(page).to have_content "product"
    expect(page).to have_content 19.99
  end

  scenario 'User can jump detail page of product to click its name' do
    click_link "Product"
    visit potepan_product_path(id: product.id)
  end
end
