RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "#show" do
    context "as a visitor" do
      let(:taxon) { create(:taxon) }
      let(:taxonomy) { create(:taxonomy) }
      let(:product) { create(:product) }

      before do
        taxon.products << product
        get :show, params: { id: taxon.id }
      end

      it "responds successfully" do
        expect(response).to be_successful
      end

      it "returns a 200 response" do
        expect(response).to have_http_status "200"
      end

      it "assigns @taxon" do
        expect(assigns(:taxon)).to eq taxon
      end

      it "assigns @taxonomies" do
        expect(assigns(:taxonomies)).to include taxonomy
      end

      it "assigns @products" do
        expect(assigns(:products)).to include product
      end

      it "renders the show template" do
        expect(response).to render_template(:show)
      end
    end
  end
end
