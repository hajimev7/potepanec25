FactoryBot.define do
  factory :product, class: 'Spree::Product' do
    id                { 1 }
    name              { "Product" }
    description       { "description" }
    price             { 19.99 }
    available_on      { 1.year.ago }
    deleted_at        { nil }
    shipping_category_id { 1 }
  end
end
