FactoryBot.define do
  factory :taxon, class: 'Spree::Taxon' do
    id                { 1 }
    name              { "taxon" }
  end
end
